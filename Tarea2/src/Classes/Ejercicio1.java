/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.Scanner; 
/**
 *
 * @author allan
 */
public class Ejercicio1 {
     public static void main(String args[]) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);  // Crea objeto de Scanner
        System.out.println("Ingrese lado a:"); // Solicitud de input al usuario
        double a = scan.nextDouble();
        System.out.println("Ingrese lado b:"); // Solicitud de input al usuario
        double b = scan.nextDouble();
        System.out.println("Ingrese lado c:"); // Solicitud de input al usuario
        double c = scan.nextDouble();
        double p=0.0,area=0.0;
        p = (a+b+c)/2;
        area=Math.sqrt(p*(p-a)*(p-b)*(p-c));
        System.out.println("El área del triángulo es "+area);
    }
}

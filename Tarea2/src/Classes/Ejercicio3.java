/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
// package Auto;
/**
 *
 * @author allan
 */
public class Ejercicio3 {
    public static void main(String[] args){
        Auto auto1=new Auto();
        auto1.setAnno(2023);
        auto1.setMarca("Lamborghini");
        auto1.setColor("Rojo");
        auto1.setModelo("Carrito");
        auto1.setNumeroSerie("A123B456C");
        auto1.setPlaca("BKX-130");
        System.out.println(auto1.getAnno());
        System.out.println(auto1.getColor());
        System.out.println(auto1.getMarca());
        System.out.println(auto1.getModelo());
        System.out.println(auto1.getNumeroSerie());
        System.out.println(auto1.getPlaca());
    }
}
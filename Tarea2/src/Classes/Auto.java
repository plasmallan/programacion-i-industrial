/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

/**
 *
 * @author allan
 */
public class Auto {
    private String placa;
    private String marca;
    private String modelo;
    private String numeroSerie;
    private String color;
    private int anno;
    public String getPlaca(){
        return placa;
    }
    public String getMarca(){
        return marca;
    }
    public String getModelo(){
        return modelo;
    }
    public String getNumeroSerie(){
        return numeroSerie;
    }
    public String getColor(){
        return color;
    }
    public int getAnno(){
        return anno;
    }
    public void setPlaca(String newPlaca){
        this.placa=newPlaca;
    }
    public void setMarca(String newMarca){
        this.marca=newMarca;
    }
    public void setModelo(String newModelo){
        this.modelo=newModelo;
    }
    public void setNumeroSerie(String newNumeroSerie){
        this.numeroSerie=newNumeroSerie;
    }
    public void setColor(String newColor){
        this.color=newColor;
    }
    public void setAnno(int newAnno){
        this.anno=newAnno;
    }
}

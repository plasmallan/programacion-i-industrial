/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author allan
 */
package Classes;
import java.util.Scanner; // para recibir input del usuario
public class Ejercicio2 {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);  // Objeto de Scanner
        System.out.println("Ingrese los grados Celsius para convertir a Fahrenheit:"); // Solicitud al usuario
        double celsius=scan.nextDouble();  // Lectura del input del usuario
        double fahrenheit = 0.0; // Variable a calcular
        fahrenheit = (celsius *9.0)/5.0 + 32.0; // Cálculo
        System.out.println(celsius+" C equivale a "+ fahrenheit+" F"); // Se imprime el resultado
    }
}

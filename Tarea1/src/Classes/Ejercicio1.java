/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author allan
 */
package Classes;
import java.util.Scanner; 
public class Ejercicio1 {
    public static void main(String args[]) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);  // Crea objeto de Scanner
        System.out.println("Ingrese número de horas:"); // Solicitud de input al usuario
        int hours = scan.nextInt();  // Lectura del input del usuario
        int week=0,day=0,hour=0; // Variables a calcular utiliza int para evitar recurrir a Math.floor, por facilidad.
        week = hours/24/7; // Calcula número de semanas
        day = hours/24%7; // Calcula número de días
        hour = hours-week*7*24-day*24; // Calcula horas restantes
        System.out.println(hours+" h equivale a "+ week+" semanas "+day+" días y "+hour+" horas."); // Imprime el resultado
    }
}

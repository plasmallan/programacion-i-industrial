/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */

/**
 *
 * @author allan
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        double celsius=50.0,fahrenheit = 0.00;
        fahrenheit = (celsius *9)/5 + 32.0;
        System.out.println(celsius+" C equivale a "+ fahrenheit+" F");
    }
}
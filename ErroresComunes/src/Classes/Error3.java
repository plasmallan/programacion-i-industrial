/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.Scanner;
/**
 *
 * @author allan
 */
public class Error3 {
    public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    double radio, area;
    System.out.println("Introduce radio de la circunferencia:");
    radio = sc.nextDouble();
    area = Math.PI * Math.pow(radio, 2);
    System.out.println("Area de la circunferencia -> " + area);
    }

}

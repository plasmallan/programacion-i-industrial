/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author allan
 */
public class UtilitarioCadena {
    public void mostrarInformacion(Persona persona) {
        StringBuffer resultado = new StringBuffer();
        
        // Agregar nombre de la persona
        resultado.append("*************************************\n");
        resultado.append("* Nombre: ").append(persona.getNombre()).append(" *\n");
        resultado.append("* ").append(getEspaciosEnBlanco(persona.getNombre().length() + 10)).append(" *\n");

        // Agregar información de contacto
        resultado.append("************** Contacto ***********\n");
        resultado.append("Correo: ").append(persona.getContacto().getEmail()).append("\n");
        resultado.append("Dirección: ").append(persona.getContacto().getDireccion()).append("\n");
        resultado.append("Teléfono: ").append(persona.getContacto().getTelefono()).append("\n");
        resultado.append("*************************************");

        // Mostrar el resultado en pantalla
        System.out.println(resultado.toString());
    }

    private String getEspaciosEnBlanco(int cantidad) {
        StringBuffer espacios = new StringBuffer();
        for (int i = 0; i < cantidad; i++) {
            espacios.append(" ");
        }
        return espacios.toString();
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author allan
 */
import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UtilitarioCadena utilitario = new UtilitarioCadena();

        // Crear contactos predefinidos
        Contacto contacto1 = new Contacto("Dirección 1", "correo1@example.com", "123456789");
        Contacto contacto2 = new Contacto("Dirección 2", "correo2@example.com", "987654321");
        Contacto contacto3 = new Contacto("Dirección 3", "correo3@example.com", "456123789");
        Contacto contacto4 = new Contacto("Dirección 4", "correo4@example.com", "789123456");
        Contacto contacto5 = new Contacto("Dirección 5", "correo5@example.com", "321654987");

        // Crear personas y asignar contactos
        Persona persona1 = new Persona("Juan", "Ramírez", "1234567890", contacto1);
        Persona persona2 = new Persona("José", "Ramírez", "0987654321", contacto2);
        Persona persona3 = new Persona("Miguel", "Ramírez", "9876543210", contacto3);
        Persona persona4 = new Persona("Andrés", "Ramírez", "4567890123", contacto4);
        Persona persona5 = new Persona("Mauricio", "Ramírez", "3216549870", contacto5);

        int opcion;
        do {
            // Mostrar menú
            System.out.println("********Libreta de direcciones*********");
            System.out.println("1. Ver contactos de la libreta");
            System.out.println("2. Salir");
            System.out.print("Ingrese una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea

            // Realizar acciones según la opción seleccionada
            switch (opcion) {
                case 1:
                    // Mostrar los contactos
                    System.out.println("******** Contactos ********");
                    utilitario.mostrarInformacion(persona1);
                    utilitario.mostrarInformacion(persona2);
                    utilitario.mostrarInformacion(persona3);
                    utilitario.mostrarInformacion(persona4);
                    utilitario.mostrarInformacion(persona5);
                    System.out.println("****************************");
                    break;
                case 2:
                    System.out.println("********Hasta Pronto*********");
                    break;
                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
                    break;
            }
        } while (opcion != 2);

        scanner.close();
    }
}

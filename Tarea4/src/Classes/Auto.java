/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

/**
 *
 * @author allan
 */
public class Auto {
    private String placa;
    private String marca;
    private String modelo;
    private String numeroSerie;
    private String color;
    private int anno;
    
    // Constructor
    public Auto(String placa, String marca, String modelo, String numeroSerie, String color, int anno){    
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.numeroSerie = numeroSerie;
        this.color = color;
        this.anno = anno;
    }
    
    // Acciones
    public void arrancar() {
        System.out.println("El auto arranca");
    }
    public void frenar(){
        System.out.println("El auto frena");
    }
    public void acelerar(){
        System.out.println("El auto acelera");
    }
    @Override
    public String toString() {
        return "Auto{" + "placa=" + placa + ", marca=" + marca + ", modelo=" + modelo + ", numeroSerie=" + numeroSerie + ", color=" + color + ", anno=" + anno + '}';
    }
    // Setters and getters
    public String getPlaca(){
        return placa;
    }
    public String getMarca(){
        return marca;
    }
    public String getModelo(){
        return modelo;
    }
    public String getNumeroSerie(){
        return numeroSerie;
    }
    public String getColor(){
        return color;
    }
    public int getAnno(){
        return anno;
    }
    public void setPlaca(String newPlaca){
        this.placa=newPlaca;
    }
    public void setMarca(String newMarca){
        this.marca=newMarca;
    }
    public void setModelo(String newModelo){
        this.modelo=newModelo;
    }
    public void setNumeroSerie(String newNumeroSerie){
        this.numeroSerie=newNumeroSerie;
    }
    public void setColor(String newColor){
        this.color=newColor;
    }
    public void setAnno(int newAnno){
        this.anno=newAnno;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.Date;
import java.time.LocalDate;
/**
 *
 * @author allan
 */
public class UtilitarioFecha {
    private Date resultadoFecha;
    private Date fechaInicio;
    private Date fechaFinal;
    private long diferencia;

    public UtilitarioFecha(Date fechaInicio, Date fechaFinal) {
        this.fechaInicio = fechaInicio;
        this.fechaFinal = fechaFinal;
    }

    public Date getResultadoFecha() {
        return resultadoFecha;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public long getDiferencia() {
        return diferencia;
    }

    public void setResultadoFecha(Date resultadoFecha) {
        this.resultadoFecha = resultadoFecha;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public void setDiferencia(long diferencia) {
        this.diferencia = diferencia;
    }

    
    
    public long calculoHoras(Date fechaInicio, Date fechaFinal) {
        long dh=fechaFinal.getTime()-fechaInicio.getTime();
        dh=dh/1000/60/60;
        setDiferencia(dh);
        return getDiferencia();
    }
    
    public long calculoDias(Date fechaInicio, Date fechaFinal) {
        long dh=fechaFinal.getTime()-fechaInicio.getTime();
        dh=dh/1000/60/60/24;
        setDiferencia(dh);
        return getDiferencia();
    }
}

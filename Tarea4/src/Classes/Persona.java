/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

/**
 *
 * @author allan
 */
public class Persona {
    private String cedula;
    private String nombre;
    private String apellidos;
    // constructor
    public Persona(String cedula, String nombre, String apellidos) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }
    
    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", apellidos=" + apellidos + '}';
    }
    
    public String getCedula(){
        return cedula;
    }
    public String getNombre(){
        return nombre;
    }
    public String getApellidos(){
        return apellidos;
    }
    public void setCedula(String newCedula){
        this.cedula=newCedula;
    }
    public void setNombre(String newNombre){
        this.nombre=newNombre;
    }
    public void setApellidos(String newApellidos){
        this.apellidos=newApellidos;
    }
}

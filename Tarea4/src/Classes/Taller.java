/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.ArrayList;
/**
 *
 * @author allan
 */
public class Taller {
    private String nombre;
    private String dirección;
    private Persona propietario;
    private ArrayList formularios;
    // constructor

    public Taller(String nombre, String dirección, Persona propietario) {
        this.nombre = nombre;
        this.dirección = dirección;
        this.propietario = propietario;
        ArrayList<Formulario> lista = new ArrayList<Formulario>();
        setFormularios(lista);
    }
    
    public void ingresoDeVehiculo() {
        System.out.println("Vehículo ingresado");
    }
    
    @Override
    public String toString() {
        return "Taller{" + "nombre=" + nombre + ", direcci\u00f3n=" + dirección + ", propietario=" + propietario + '}';
    }
    // Setters and getters
    public String getNombre() {
        return nombre;
    }
    public String getDirección() {
        return dirección;
    }
    public Persona getPropietario() {
        return propietario;
    }
    public ArrayList getFormularios() {
        return formularios;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setDirección(String dirección) {
        this.dirección = dirección;
    }
    public void setPropietario(Persona propietario) {
        this.propietario = propietario;
    }
    public void setFormularios(ArrayList formularios) {
        this.formularios = formularios;
    }
    
    public void añadirFormulario(Formulario formulario){
        this.formularios.add(formulario);
    }
}

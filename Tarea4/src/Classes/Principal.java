/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.Date;

/**
 *
 * @author allan
 */
public class Principal {
    public static void main(String[] args) {
        Date fechaInicio=new Date(2023,4,7);
        Date fechaFinal=new Date(2023,4,17);
        UtilitarioFecha r =new UtilitarioFecha(fechaInicio,fechaFinal);
        System.out.println(r.calculoHoras(fechaInicio,fechaFinal));
        System.out.println(r.calculoDias(fechaInicio,fechaFinal));
        Persona persona=new Persona("117160504","Juan","Valdez");
        
        Taller taller=new Taller(persona.getNombre(),"Cartago",persona);
        Auto carrito=new Auto("BKX130","Hyundai","Creta","AG123","Azul",2012);
        Formulario formulario=new Formulario(165420,"Carrito",fechaInicio,fechaFinal,"Cambio de filtros",carrito);
        taller.añadirFormulario(formulario);
        System.out.println(taller.getFormularios());
    }
}

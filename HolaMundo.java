/**
* @(#)HolaMundo.Java
*
* @author Laura Guzmán
* @version 1.00 2009/9/27
*/
public class HolaMundo {
/**
* Creates a new instance of <code>HolaMundo</code>.
/**
* @param args the command line arguments
*/
    public static void main(String[] args) {		
		System.out.println("Hola Mundo");
    }
}
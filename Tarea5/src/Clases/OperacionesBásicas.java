/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clases;
import java.util.Scanner;
/**
 *
 * @author allan
 */
public class OperacionesBásicas {
    public void solicitudDatos() {
        Scanner scanner = new Scanner(System.in);

        int opcion;
        do {
            System.out.println("1. Suma");
            System.out.println("2. Resta");
            System.out.println("3. Multiplicación");
            System.out.println("4. División");
            System.out.println("5. Salir");
            System.out.print("Selecciona una de las opciones: ");
            opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea

            if (opcion >= 1 && opcion <= 4) {
                System.out.print("Digite número 1: ");
                double numero1 = scanner.nextDouble();
                System.out.print("Digite número 2: ");
                double numero2 = scanner.nextDouble();
                scanner.nextLine(); // Consumir el salto de línea

                switch (opcion) {
                    case 1:
                        double suma = numero1 + numero2;
                        System.out.println("El resultado de la suma es: " + suma);
                        break;
                    case 2:
                        double resta = numero1 - numero2;
                        System.out.println("El resultado de la resta es: " + resta);
                        break;
                    case 3:
                        double multiplicacion = numero1 * numero2;
                        System.out.println("El resultado de la multiplicación es: " + multiplicacion);
                        break;
                    case 4:
                        if (numero2 != 0) {
                            double division = numero1 / numero2;
                            System.out.println("El resultado de la división es: " + division);
                        } else {
                            System.out.println("No se puede dividir entre cero.");
                        }
                        break;
                }
            } else if (opcion == 5) {
                System.out.println("Hasta pronto");
            } else {
                System.out.println("Opción inválida. Por favor, selecciona una opción válida.");
            }

            System.out.println();
        } while (opcion != 5);

        scanner.close();
    }
}

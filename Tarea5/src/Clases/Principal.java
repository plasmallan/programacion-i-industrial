/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clases;

/**
 *
 * @author allan
 */
public class Principal {
    public static void main(String[] args) {
        System.out.print("Números primos del 1 al 100: ");

        for (int i = 2; i <= 100; i++) {
            if (Primo.esPrimo(i)) {
                System.out.print(i);
                if (i != 100) {
                    System.out.print(", ");
                }
            }
        }
        OperacionesBásicas operaciones = new OperacionesBásicas();
        operaciones.solicitudDatos();
    }
}

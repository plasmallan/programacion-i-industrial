/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;
import java.util.Date;

/**
 *
 * @author allan
 */
public class Formulario {
    private int codigoEntrada;
    private String nombre;
    private Date fechaIngreso;
    private Date fechaSalida;
    private String descripciónVehículo;
    private Auto auto;

    @Override
    public String toString() {
        return "Formulario{" + "codigoEntrada=" + codigoEntrada + ", nombre=" + nombre + ", fechaIngreso=" + fechaIngreso + ", fechaSalida=" + fechaSalida + ", descripci\u00f3nVeh\u00edculo=" + descripciónVehículo + ", auto=" + auto + '}';
    }
    
    // Constructors

    public Formulario(int codigoEntrada, String nombre, Date fechaIngreso, Date fechaSalida, String descripciónVehículo, Auto auto) {
        this.codigoEntrada = codigoEntrada;
        this.nombre = nombre;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.descripciónVehículo = descripciónVehículo;
        this.auto = auto;
    }

    
    public Formulario(){
        fechaIngreso=new Date();
        fechaSalida=new Date();
    }
    
    // Setters and getters
    public int getCodigoEntrada(){
        return codigoEntrada;
    }
    public String getNombre(){
        return nombre;
    }
    public Date getFechaIngreso(){
        return fechaIngreso;
    }
    public Date getFechaSalida(){
        return fechaSalida;
    }
    public String getDescripciónVehículo(){
        return descripciónVehículo;
    }
    public Auto getAuto(){
        return auto;
    }
    public void setCodigoEntrada(int newCodigoEntrada){
        this.codigoEntrada=newCodigoEntrada;
    }
    public void setNombre(String newNombre){
        this.nombre=newNombre;
    }
    public void setFechaIngreso(Date newFechaIngreso){
        this.fechaIngreso=newFechaIngreso;
    }
    public void setFechaSalida(Date newFechaSalida){
        this.fechaSalida=newFechaSalida;
    }
    public void setDescripciónVehículo(String newDescripciónVehículo){
        this.descripciónVehículo=newDescripciónVehículo;
    }
    public void setAuto(Auto newAuto){
        this.auto=newAuto;
    }
}

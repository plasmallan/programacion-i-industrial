/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

/**
 *
 * @author allan
 */
public class Taller {
    private String nombre;
    private String dirección;
    private String propietario;
    // constructor

    public Taller(String nombre, String dirección, String propietario) {
        this.nombre = nombre;
        this.dirección = dirección;
        this.propietario = propietario;
    }
    
    @Override
    public String toString() {
        return "Taller{" + "nombre=" + nombre + ", direcci\u00f3n=" + dirección + ", propietario=" + propietario + '}';
    }
    // Setters and getters
    public String getNombre(){
        return nombre;
    }
    public String getDirección(){
        return dirección;
    }
    public String getPropietario(){
        return propietario;
    }
    public void setNombre(String newNombre){
        this.nombre=newNombre;
    }
    public void setDirección(String newDirección){
        this.dirección=newDirección;
    }
    public void setPropietario(String newPropietario){
        this.propietario=newPropietario;
    }
}
